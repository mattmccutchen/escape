#!/bin/bash
set -e

BASE=https://mattmccutchen.net/escape/app-downloads/
CC_LIB_REV=3957
SDL_VER=1.2.15
SDL_NET_VER=1.2.8
SDL_MIXER_VER=1.2.12

all=false
upstream=false
while [ $# -gt 0 ] ; do
    case "$1" in
        # --all: Download sources for GPL compliance even if they are not needed
        # for the build on the current OS.
        (--all) all=true;;
        # --upstream: Download sources from their respective upstreams instead
        # of mattmccutchen.net . Mainly to test that the upstream links are
        # correct. In some cases, this requires additional tools (svn client for
        # cc-lib).
        (--upstream) upstream=true;;
        (*) echo "Unrecognized argument $1" 2>&1; exit 1;;
    esac
    shift
done

# download_and_extract_tar ARCHIVE_URL
function download_and_extract_tar {
    local archive_url="$1" archive_name="$(basename "$1")"
    curl --fail "$archive_url" -o "$archive_name"
    # Rely on tar to invoke the correct decompressor.
    tar "${EXTRA_TAR_OPTS[@]}" -xf "$archive_name"
}

# download_and_extract ARCHIVE_NAME EXTRACTED_DIR DESIRED_DIR UPSTREAM_CMD
function download_and_extract {
    local archive_name="$1" extracted_dir="$2" desired_dir="$3"
    shift 3
    echo "=== Downloading $desired_dir ..."
    rm -rf "$extracted_dir" "$desired_dir"
    if $upstream; then
        "$@"
    else
        download_and_extract_tar "$BASE/$archive_name"
    fi
    mv "$extracted_dir" "$desired_dir"
}

OS="$(make print-os)"

download_and_extract cc-lib-r${CC_LIB_REV}.tar.gz cc-lib-r${CC_LIB_REV} cc-lib \
                     svn export -r ${CC_LIB_REV} https://svn.code.sf.net/p/tom7misc/svn/trunk/cc-lib cc-lib-r${CC_LIB_REV}
cat <<EOM
NOTICE: You have to manually move cc-lib so it is located at ../cc-lib .
This is not done automatically because we don't want to touch the parent
directory without your consent; maybe you want to move the Escape working
tree to a subdirectory before putting cc-lib alongside it.
EOM
# In the future, I plan to make Escape build against cc-lib located inside
# the working tree.

if [ "$OS" == WIN32 ] || $all; then
    download_and_extract SDL-${SDL_VER}.tar.gz SDL-${SDL_VER} SDL \
                         download_and_extract_tar https://libsdl.org/release/SDL-${SDL_VER}.tar.gz
    # TODO: investigate
    patch -p0 <SDL.patch
    download_and_extract SDL_net-${SDL_NET_VER}.tar.gz SDL_net-${SDL_NET_VER} SDL_net \
                         download_and_extract_tar https://libsdl.org/projects/SDL_net/release/SDL_net-${SDL_NET_VER}.tar.gz
    # "Xcode" dir contains symlinks, which break msys tar, and we don't need it.
    (
    EXTRA_TAR_OPTS=(--exclude Xcode)
    download_and_extract SDL_mixer-${SDL_MIXER_VER}.tar.gz SDL_mixer-${SDL_MIXER_VER} SDL_mixer \
                         download_and_extract_tar https://libsdl.org/projects/SDL_mixer/release/SDL_mixer-${SDL_MIXER_VER}.tar.gz
    )
fi
