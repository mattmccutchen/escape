I currently have no fixed policies for issues or merge requests; I will establish
some if and when I start receiving poorly thought out contributions.  In fact,
you are welcome to use the issue tracker for any discussion related to my version
of Escape, not just issues.  However, the usual warning applies that you should
check with me before putting significant effort into a code change based on the
hope that I will merge it.  (Of course, consider that I may add a link from my
web site to your version even if I don't merge your change.)

~ Matt McCutchen
