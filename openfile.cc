/* The Windows headers have various conflicts with Escape's headers, so put this
 * in its own file that includes no Escape headers.
 *
 * TODO: Can we use a different API on Windows? */

#include "openfile.h"

#if defined(LINUX)
/* fork, execlp, exit */
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#elif defined(WIN32)
/* ShellExecuteA */
#define WINVER _WIN32_WINNT_WINXP
#include <windows.h>
#include <winnt.h>
#include <shellapi.h>
#endif

void TryOpenFile(const char *file) {
#if defined(LINUX)
  if (fork() == 0) {
    execlp("xdg-open", "xdg-open", file, nullptr);
    exit(1);
  }
#elif defined(WIN32)
  ShellExecuteA(NULL, "open", file, nullptr, nullptr, SW_SHOWNORMAL);
  /* ignore result */
#else
  #error "ShowHelp not implemented on this OS"
#endif
}
