
#ifndef __PLAY_H
#define __PLAY_H

#include "level.h"
#include "player.h"
#include "dirt.h"
#include "draw.h"

/* XXX clean this up ! */

enum class PlayResultType {
  QUIT, ERROR, SOLVED, EXIT,
};

/* XXX move ... */
struct PlayResult {
  PlayResultType type;
  Solution sol;

  static PlayResult Quit() {
    PlayResult p;
    p.type = PlayResultType::QUIT;
    return p;
  }

  static PlayResult Error() {
    PlayResult p;
    p.type = PlayResultType::ERROR;
    return p;
  }

  static PlayResult Solved(Solution s) {
    PlayResult p;
    p.type = PlayResultType::SOLVED;
    p.sol = std::move(s);
    return p;
  }
};

struct Play : public Drawable {
  // Does not take ownership of level.
  static Play *Create(const Level *l);

  // For this one, the caller is responsible for making sure allowrate = false
  // if the level is not in a web collection.  allowrate is required to remind
  // callers they are responsible for this.
  //
  // saved may be nullptr if the caller isn't interested in preserving a
  // checkpoint across calls to DoPlaySaveRecord.
  //
  // Warning: This mutates the internally saved level, so it shouldn't be called
  // more than once.  This could probably use redesign.
  virtual void DoPlaySaveRecord(Player *p, Solution *saved,
                                const string &md5, bool allowrate,
                                bool show_intro = false) = 0;
  /* play, recording the game in the player's solution file */
  static void PlayRecord(const string &file, Player *plr,
			 bool allowrate = true,
                         bool show_intro = false);
  void Draw() override = 0;
  void ScreenResize() override = 0;
  virtual ~Play();
};

#endif
